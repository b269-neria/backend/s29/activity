// Find users with s in their first name or d in their last name while displaying only firstName and lastName also without displaying the _id
db.users.find(
	{ $or: 
	[
		{firstName:{$regex: "S", $options: "i"}}, 
		{lastName:{$regex: "D", $options: "i"}}
	]
},
{
	firstName: 1,
	lastName: 1,
	_id: 0
}
);

// Find user from the HR department and their age is greater than or equal to 70

db.users.find(
	{
		$and:
		[
			{department: "HR"},
			{age: {$gte: 70}}
		]
	}
	);

// Find users with the letter e in their first name AND has an age of less than or equal to 30
db.users.find(
	{
		$and:
		[
			{firstName:{$regex: "e", $options: "i"}}, 
			{age: {$lte: 30}}
		]
	}
	)
